# simplehelp-alerting

Scripts to deal with the output from Simple Help's "send to website" functionality

## Microsoft Teams
The teams.php file deals with Microsoft Teams.

To make this work, set up a new connector in a Teams channel: https://docs.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook

Edit the $url variable in the file so that it is your webhook URL.

Host the PHP file somewhere, and configure your alerts in Simple Help to point to this PHP file. Any alert will appear in your Teams channel.
