<?php

$url = '<insert webhook URL here>';

$rawdata = file_get_contents("php://input");

if (strpos($rawdata, 'AlertTriggerEvent') !== false) {
	$image = 'https://alwaysnetworks.co.uk/wp-content/uploads/2021/01/bad.png';
	$title = 'New Alert from RMM';
} else if (strpos($rawdata, 'AlertResetEvent') !== false) {
	$image = 'https://alwaysnetworks.co.uk/wp-content/uploads/2021/01/good.png';
	$title = 'Alert cleared';
} else {
	exit("Invalid request");
}


$xml = simplexml_load_string($rawdata);
$json = json_encode($xml);
$array = json_decode($json, true);

$ch = curl_init();

$jsonData = array(
	"@type" => "MessageCard",
	"@context" => "http://schema.org/extensions",
	"themeColor" => "2c3364",
	"summary" => $title,
	"sections"=> array(

		)
);

$facts = array();
foreach ($array as $tag => $val) {
	array_push($facts, array(
			"name" => $tag,
			"value" => $val
		)
	);
}

$section = array(
	"activityTitle" => $title,
	"activityImage" => $image,
	"facts" => $facts
);
array_push($jsonData["sections"], $section);

$jsonDataEncoded = json_encode($jsonData, true);

$header = array();
$header[] = 'Content-type: application/json';

curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

$result = curl_exec($ch);
curl_close($ch);

?>
